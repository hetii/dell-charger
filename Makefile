# Based on project: https://github.com/KivApple/dell-charger-emulator

TARGET=attiny45
HFUSE=0xd7
LFUSE=0xd2

ISP=stk500v2
PORT=/dev/ttyACM0
F_CPU=8000000UL

all: clean main.hex
	@echo Done...
	
help:
	@echo "Usage: make                same as make help"
	@echo "       make help           same as make"
	@echo "       make main.hex       create main.hex"
	@echo "       make clean          remove redundant data"
	@echo "       make disasm         disasm main"
	@echo "       make flash          upload main.hex into flash"
	@echo "       make fuses          program fuses"
	@echo "       make avrdude        test avrdude"
	@echo
	@echo "Current values:"
	@echo "       TARGET=${TARGET}"
	@echo "       LFUSE=${LFUSE}"
	@echo "       HFUSE=${HFUSE}"
	@echo "       CLOCK=${F_CPU}"
	@echo "       ISP=${ISP}"
	@echo "       PORT=${PORT}"

OBJECTS = crc8.o main.o

COMPILE = avr-gcc -Wall -O2 -DF_CPU=${F_CPU} -I. -mmcu=$(TARGET)

.c.o:
	$(COMPILE) -c $< -o $@
.S.o:
	$(COMPILE) -x assembler-with-cpp -c $< -o $@
.c.s:
	$(COMPILE) -S $< -o $@

clean:
	rm -f *.hex *.lst *.obj *.cof *.list *.map *.eep.hex *.bin *.o *.s 

# file targets:
main.bin:	$(OBJECTS)
	$(COMPILE) -o main.bin $(OBJECTS) -Wl,-Map,main.map

main.hex: main.bin
	rm -f main.hex main.eep.hex
	avr-objcopy -j .text -j .data -O ihex main.bin main.hex

disasm:	main.bin
	avr-objdump -d main.bin

cpp:
	$(COMPILE) -E main.c

flash:
	sudo avrdude -V -e -c ${ISP} -p ${TARGET} -P ${PORT} -U flash:w:main.hex

fuses:
	sudo avrdude -c ${ISP} -p ${TARGET} -P ${PORT} -u -U hfuse:w:$(HFUSE):m -U lfuse:w:$(LFUSE):m

avrdude:
	sudo avrdude -V -v -c ${ISP} -p ${TARGET} -P ${PORT}

prepare:
	sudo apt-get install gcc-avr avr-libc avrdude
